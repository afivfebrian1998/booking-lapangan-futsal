-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2021 at 04:15 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `booking_lapangan_futsal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(20) NOT NULL,
  `nama_admin` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama_admin`, `email`, `password`) VALUES
(1, 'Afiv', 'afivfebrian@gmail.com', 'qwerty');

-- --------------------------------------------------------

--
-- Table structure for table `menu_lapangan`
--

CREATE TABLE `menu_lapangan` (
  `id` int(10) NOT NULL,
  `tipe_lapangan` varchar(30) NOT NULL,
  `waktu` int(10) NOT NULL,
  `harga` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu_lapangan`
--

INSERT INTO `menu_lapangan` (`id`, `tipe_lapangan`, `waktu`, `harga`) VALUES
(1, 'sintetis', 1, 80000),
(2, 'sintetis', 2, 160000),
(3, 'sintetis', 3, 240000),
(4, 'sintetis', 4, 320000),
(5, 'non sintetis', 1, 50000),
(6, 'non sintetis', 2, 100000),
(9, 'non sintetis', 3, 150000),
(10, 'non sintetis', 4, 200000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(10) NOT NULL,
  `nama_pemesan` varchar(30) NOT NULL,
  `kode_booking` varchar(30) NOT NULL,
  `tipe_lapangan` varchar(20) NOT NULL,
  `timming` varchar(20) NOT NULL,
  `total_bayar` int(30) NOT NULL,
  `waktu_pemesanan` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `pembayaran` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `nama_pemesan`, `kode_booking`, `tipe_lapangan`, `timming`, `total_bayar`, `waktu_pemesanan`, `status`, `pembayaran`) VALUES
(33, 'afiv', 'BOK001', 'sintetis', '3', 240000, '01/03/2021 9:58 PM', 'sudah disetujui', 'sudah dibayar');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `jenis_kelamin` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `alamat`, `jenis_kelamin`, `email`, `password`) VALUES
(14, 'Afiv Febrian', 'Kota Bogor', 'Laki-laki', 'afivfebrian1998@gmail.com', 'asdfg'),
(15, 'doni', 'Kota Bogor', 'Laki-laki', 'afivfebrian1998@gmail.com', '12345'),
(16, 'doni', 'Kota Bogor', 'Laki-laki', 'afivfebrian1998@gmail.com', ''),
(17, '', '', '', '', ''),
(18, '', '', '', '', ''),
(20, 'topik', 'Jakarta', 'Laki-laki', 'topik@gmail.com', 'topik');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_lapangan`
--
ALTER TABLE `menu_lapangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_lapangan`
--
ALTER TABLE `menu_lapangan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
