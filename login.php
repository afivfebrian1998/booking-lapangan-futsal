<!DOCTYPE html>
<html>
<head>
  <title>LOGIN</title>
  <link rel="stylesheet" type="text/css" href="login.css">

</head>
<body>
  <h1>SELAMAT DATANG DI GEJAYAN FUTSAL</h1>
  <h1>silahkan login untuk mendaftar</h1>
  <div class="kotak_login">
    <p class="tulisan_login">Silahkan login</p>
 
    <form action="proseslogin.php" method="POST">
      <label>Email</label>
      <input type="text" name="email" class="form_login" placeholder="Masukkan email ..">
 
      <label>Password</label>
      <input type="password" name="password" class="form_login" placeholder="Password ..">
 
      <input type="submit" class="tombol_login" value="LOGIN">
 
      <br/>
      <br/>
      <center>
        <a class="link" href="daftar/formdaftar.html">Belum punya akun? Silahkan daftar</a>
      </center>
    </form>
    
  </div>
 
 
</body>
</html>